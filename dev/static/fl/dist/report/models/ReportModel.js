define([
    'jquery',
    'underscore',
    'backbone',
    'apiUrls',
], function ($, _, Backbone, apiUrls) {

    var ReportModel = Backbone.Model.extend({
        defaults: {
            report: {
                name:"",
                lastUpdateDate:"",
                summaryToDate:[]
            }
        },

        /**
         *
         * @param reportId
         */
        setReportId: function(reportId){
            this.set('reportId', reportId);
        },

        /**
         *
         * @param reportId
         */
        updateModel: function (reportId) {
            this.setReportId(reportId);
            var self = this;

            $.ajax({
                url: apiUrls.reportView,
                type: 'GET',
                contentType: 'application/json',
                data:{
                    reportId: reportId
                },
                dataType: 'json',
                success: function (data) {
                    self.set('report', data);
                },
            });

        }


    });

    return ReportModel;

});