define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'ReportPageView',
], function ($, _, Backbone, frontUrls, ReportPageView) {
    var ReportRoutes = Backbone.Router.extend({
        routes: frontUrls.report,


        report: function (reportId) {
            $('.js-header-loader').removeClass('hide');
            var view = new ReportPageView();
            view.setReportId(reportId);
            console.log('report ' + reportId);
        },


    });

    return new ReportRoutes();
});