define([
    'jquery',
    'Trello',
    'materialize',

/********VIEWS*********/
    'HeaderView',

/********ROUTES********/
    'IndexRoute',
    'AuthRoutes',
    'DashboardRoutes',
    'ReportCreateRoutes',
    'ReportRoutes'

], function ($, Trello, materialize, headerView, indexRoute, authRoutes, DashboardRoutes, ReportCreateRoutes, ReportRoutes) {

    function Application() {

    }

    Application.prototype.init = function () {
        this.backboneHistoryStart();
        this.initBaseViews();
        console.log('App did load');
    }

    Application.prototype.backboneHistoryStart = function () {
        //Backbone.history.start({pushState: true, root: app.root});
        var app = {root: "/"};
        Backbone.history.start({pushState: true});

        $(document).on("click", "a[href]:not([data-bypass])", function (evt) {
            var href = {prop: $(this).prop("href"), attr: $(this).attr("href")};
            var root = location.protocol + "//" + location.host + app.root;

            if (href.prop.slice(0, root.length) === root) {
                evt.preventDefault();
                $("html, body").animate({scrollTop: 0}, "fast");
                Backbone.history.navigate(href.attr, true);
            }
        });

    }

    Application.prototype.initBaseViews = function () {
        console.log('Init base views');
        headerView.render();
    }

    return new Application();

});
