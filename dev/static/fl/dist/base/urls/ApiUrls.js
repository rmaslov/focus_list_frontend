define([],function(){
    var apiUrls = {
        me:             "/api/v1/auth/me",
        authByEmail:    "/api/v1/auth/email",
        authByTrello:   "/api/v1/auth/trello",
        logout:   "/api/v1/logout",

        reportCreateOrgBoard: "/api/v1/reports/create/trello-org-boards",
        reportCreateListForBoard: "/api/v1/reports/create/trello-list-for-boards",
        reportCreate: '/api/v1/reports/create/done',

        reportAll: '/api/v1/reports/all',
        reportView:'/api/v1/reports/view',
    };

    return apiUrls;
});