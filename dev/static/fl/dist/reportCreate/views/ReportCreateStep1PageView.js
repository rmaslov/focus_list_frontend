define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'ReportCreateModel',

    'text',
    'text!/static/fl/js/app/reportCreate/templates/step1.html'
], function ($, _, Backbone, frontUrls, reportCreateModel ,text, stepTemplate) {
    var ReportCreateStep1PageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(stepTemplate),
        model: reportCreateModel,

        events: {
            'click .js-report-create-table-board': 'boardClicked',
            'click .js-report-create-go-to-step-2': 'goToStep2'
        },

        boardClicked: function (ev) {
            console.log("'click .js-report-create-table-board': 'boardClicked'");
            console.log($(ev.currentTarget).attr('data-id'));
            this.model.addBoardToReport($(ev.currentTarget).attr('data-id'));
            $('#js-rc-board-' + $(ev.currentTarget).attr('data-id')).click();
        },

        goToStep2: function(ev){
            console.log('click .js-report-create-go-to-step-2:goToStep2');
            Backbone.history.navigate('/report-create/step2', true);
        },

        initialize: function () {
            this.model.on('change', this.render, this);

            this.model.loadTrelloOrganizationAndBoards();
        },

        dest:function(){
            this.model.off('change', this.render, this);
            this.undelegateEvents();
            //this.remove();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this.$el;
        },


    });

    return ReportCreateStep1PageView  ;
});