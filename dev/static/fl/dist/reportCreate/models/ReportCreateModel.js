define([
    'jquery',
    'underscore',
    'backbone',
    'apiUrls',
], function ($, _, Backbone, apiUrls) {

    var ReportCreateModel = Backbone.Model.extend({
        defaults: {
            organizations: [],
            boards: [],
            lists: []
        },

        clear: function () {
            console.log('ReportCreateModel.clear did run');
            this.set({});
        },

        addBoardToReport: function(boardId){
            var allBoards = this.get('boards')
            for(var key in allBoards){
                if(allBoards[key].id == boardId){
                    allBoards[key]['checked'] = true;
                }
            }
            this.attributes['boards'] = allBoards;
        },

        setListType:function(listId, typeString){
            var lists = this.get('lists')
            for(var key in lists){
                if(lists[key].id == listId){
                    if( lists[key]['types'] == null ){
                        lists[key]['types'] = [typeString];
                    }else{
                        lists[key]['types'].push(typeString);
                    }
                }
            }
            this.attributes['lists'] = lists;
        },

        removeListType:function(listId, typeString){
            var lists = this.get('lists')
            for(var key in lists){
                if(lists[key].id == listId){
                    var types = lists[key]['types'];
                    var newTypes = [];
                    for(var tKey in types){
                        if(types[tKey] != typeString){
                            newTypes.push(types[tKey]);
                        }
                    }
                    lists[key]['types'] = newTypes;
                }
            }
            this.attributes['lists'] = lists;
        },

        loadTrelloOrganizationAndBoards: function () {
            $('.js-header-loader').removeClass('hide');
            var self = this;
            $.get(apiUrls.reportCreateOrgBoard,
                {},
                function (data) {
                    self.set('organizations',data['organisations']);
                    self.set('boards',data['boards']);
                    $('.js-header-loader').addClass('hide');
                }
            );
        },

        loadTrelloListForBoards: function(){
            $('.js-header-loader').removeClass('hide');
            var self = this;
            var boardsId = [];
            var allBoards = this.get('boards')
            for(var key in allBoards){
                if(allBoards[key]['checked'] == true){
                    boardsId.push(allBoards[key]['id']);
                }
            }
            $.get(apiUrls.reportCreateListForBoard,
                {'boardIds':boardsId},
                function (data) {
                    self.set('lists', data['lists']);
                    $('.js-header-loader').addClass('hide');
                }
            );
        },

        createReport:function(){
            var request = {
                types:['ToDo', 'Doing', 'Done'],
                lists:this.get('lists')
            };

            $.ajax({
                url: apiUrls.reportCreate,
                type: 'POST',
                contentType:'application/json',
                data: JSON.stringify(request),
                dataType:'json',
                success: function(data){
                    //On ajax success do this
                    alert(data);
                },
            });

        }


    });

    return new ReportCreateModel();

});