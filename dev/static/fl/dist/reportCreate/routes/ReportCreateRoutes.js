define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'ReportCreateStep1PageView',
    'ReportCreateStep2PageView'
], function ($, _, Backbone, frontUrls, ReportCreateStep1PageView, ReportCreateStep2PageView) {
    var ReportCreateRoutes = Backbone.Router.extend({
        routes: frontUrls.create,
        step1View:null,
        step2View:null,

        step1: function () {
            console.log('step1 did load');
            this.step1View = new ReportCreateStep1PageView();
            this.step1View.render();
            console.log('step1 did load');
        },

        step2: function () {
            this.step1View.dest();
            this.step2View = new ReportCreateStep2PageView();
            this.step2View.render();
            console.log('step2 did load');
        },

    });

    return new ReportCreateRoutes();
});