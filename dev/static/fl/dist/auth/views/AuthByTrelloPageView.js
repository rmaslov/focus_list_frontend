define([
    'jquery',
    'underscore',
    'backbone',

    'AuthByTrelloModel',
    'frontUrls',

    'text',
    'text!/static/fl/js/app/auth/templates/authByTrelloPage.html'
], function ($, _, Backbone, AuthByTrelloModel, frontUrls, text, authByTrelloPage) {
    var AuthByTrelloPageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(authByTrelloPage),
        model: new AuthByTrelloModel(),

        events: {
        },

        initialize: function () {
            this.model.on('change', this.checkModel, this);
        },

        render: function () {
            this.$el.html(this.template({}));
            $('.js-auth-error').hide();
            $('.js-auth-waiting').show();
            $('.js-auth-success').hide();
            this.model.startAuth();
            return this.$el;
        },

        checkModel: function () {
            console.log('CheckModel:');
            if (this.model.get('status') == 0) {
                $('.js-auth-error').hide();
                $('.js-auth-waiting').show();
                $('.js-auth-success').hide();
            } else  if (this.model.get('status') == 1) {
                $('.js-auth-error').hide();
                $('.js-auth-waiting').hide();
                $('.js-auth-success').show();
                Backbone.history.navigate('dashboard', true);
            } else {
                $('.js-auth-error').show();
                $('.js-auth-waiting').hide();
                $('.js-auth-success').hide();
            }
        },

        authByEmail: function () {
            console.log('authByEmail:');
            var email = $('#js-email').val();
            var password = $('#js-password').val();
            this.model.login(email, password);
        }
    });

    return AuthByTrelloPageView ;
});