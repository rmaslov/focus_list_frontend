define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'text',
    'text!/static/fl/js/app/dashboard/templates/dashboard.html',
    'DashboardModel'
], function ($, _, Backbone, frontUrls, text, dashboardTemplate,DashboardModel) {
    var DashboardPageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(dashboardTemplate),
        model: new DashboardModel(),

        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.updateModel();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this.$el;
        },

    });

    return DashboardPageView ;
});