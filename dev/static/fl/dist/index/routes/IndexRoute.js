define([
    'jquery',
    'underscore',
    'backbone',
    'IndexPageView'
], function ($, _, Backbone, IndexPageView) {
    var IndexRoute = Backbone.Router.extend({

        routes: {
            "": "index",
            "index.html":"indexHtml"
        },

        index: function(){
            var indexPageView = new IndexPageView();
            indexPageView.render();
            console.log("Index page did load");
        },

        indexHtml: function(){
            console.log("Index html page did load");
            this.navigate('/', {trigger: true});
        }

    });

    return new IndexRoute();
});