define([
    "jquery",
    "underscore",
    "backbone",
    "IndexBannerView"
],function($, _, Backbone, IndexBannerView){
    var IndexPageView = Backbone.View.extend({
        el: $('.js-main-body'),
        indexBannerView: new IndexBannerView(),

        render: function(){
            this.$el.html('');
            this.$el.html(this.indexBannerView.render().$el);
            return this;
        }

    });

    return IndexPageView;
});