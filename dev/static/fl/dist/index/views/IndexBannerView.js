define([
    "jquery",
    "underscore",
    "backbone",
    "MeModel",
    "text",
    "text!/static/fl/js/app/index/templates/indexBanner.html"
], function ($, _, Backbone, meModel, text, indexBanner) {
    var IndexBannerView = Backbone.View.extend({
        template: _.template(indexBanner),
        model: meModel,

        initialize: function () {
            this.model.on('change', this.render, this);
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        }

    });

    return IndexBannerView;
});