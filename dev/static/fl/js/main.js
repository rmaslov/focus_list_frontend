// Filename: main.js

require.config({
    shim: {
        Trello: {
            deps: ['jquery'],
            exports: "Trello"
        },
        velocity: {
            deps: ['jquery'],
        },
        hammer: {
            deps: ['jquery', 'velocity'],
        },

        materialize: {
            deps: ['jquery', 'hammer'],
        },

        d3: {
            exports: "d3"
        },

        c3: {
            deps: ['d3'],
            exports: "c3"
        }
    },
    paths: {
        d3: 'frameworks/c3/d3',
        c3: 'frameworks/c3/c3',
        jquery: 'frameworks/jquery/jquery-2.1.4',
        underscore: 'frameworks/underscore/underscore-min',
        backbone: 'frameworks/backbone/backbone-min',
        velocity: 'frameworks/materialeze/velocity.min',
        hammer: 'frameworks/materialeze/hammer.min',
        materialize: 'frameworks/materialeze/materialize.min',
        Trello: 'https://api.trello.com/1/client.js?key=4261771a2e5dad3cf86b56923f209be8',


        app: 'app/base/app',
        apiUrls: 'app/base/urls/ApiUrls',
        frontUrls: 'app/base/urls/FrontUrls',

        /**Base**/
        HeaderView: 'app/base/submodules/header/views/HeaderView',
        MeModel: 'app/auth/models/MeModel',

        /**index**/
        IndexRoute: 'app/index/routes/IndexRoute',
        IndexPageView: 'app/index/views/IndexPageView',
        IndexBannerView: 'app/index/views/IndexBannerView',

        /**AUTH**/
        AuthRoutes: 'app/auth/routes/AuthRoutes',
        AuthByEmailModel: 'app/auth/models/AuthByEmailModel',
        AuthByTrelloModel: 'app/auth/models/AuthByTrelloModel',
        AuthByEmailPageView: 'app/auth/views/AuthByEmailPageView',
        AuthByTrelloPageView: 'app/auth/views/AuthByTrelloPageView',

        /**DASHOARD**/
        DashboardRoutes:'app/dashboard/routes/DashboardRoutes',
        DashboardPageView:'app/dashboard/views/DashboardPageView',
        DashboardModel:'app/dashboard/models/DashboardModel',

        /**REPORT CREATE**/
        ReportCreateRoutes:'app/reportCreate/routes/ReportCreateRoutes',
        ReportCreateStep1PageView:'app/reportCreate/views/ReportCreateStep1PageView',
        ReportCreateStep2PageView:'app/reportCreate/views/ReportCreateStep2PageView',
        ReportCreateModel:'app/reportCreate/models/ReportCreateModel',

        /**REPORT**/
        ReportRoutes:'app/report/routes/ReportRoutes',
        ReportPageView:'app/report/views/ReportPageView',
        ReportModel:'app/report/models/ReportModel',


    }

});

require([
    'app'
], function (app) {
    $(function () {
        app.init();
    });
});