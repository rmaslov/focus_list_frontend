define([
    "jquery",
    "underscore",
    "backbone",
    "materialize",
    "MeModel",

    "text",
    'text!/static/fl/js/app/base/submodules/header/templates/guest.html',
    'text!/static/fl/js/app/base/submodules/header/templates/loggedUser.html'
],function($, _, Backbone,materialize, meModel, text, guest, loggedUser){
    var HeaderView = Backbone.View.extend({
        el: $(".js-navbar"),
        guestTemplate: _.template(guest),
        loggedUserTemplate: _.template(loggedUser),
        model: meModel,

        events: {
            "click .js-logout-btn": "logout"
        },

        /**
         *
         */
        initialize: function(){
            this.model.on('change', this.render, this);
            this.render();
        },

        render: function(){
            if( this.model.get('code').code == 0){
                this.renderLoggedUser();
            }else{
                this.renderGuiest();
            }
        },

        logout: function(){
            this.model.logout();
        },

        /**
         *
         */
        renderLoggedUser: function(){
            // Compile the template using underscore
            var template = this.loggedUserTemplate( {} );
            // Load the compiled HTML into the Backbone "el"
            this.$el.html( template );
            $(".js-header-logged-user-dropdown-button").dropdown();
        },

        /**
         *
         */
        renderGuiest: function(){
            // Compile the template using underscore
            var template = this.guestTemplate( {} );
            // Load the compiled HTML into the Backbone "el"
            this.$el.html( template );
        }
    });

    return new HeaderView();
});