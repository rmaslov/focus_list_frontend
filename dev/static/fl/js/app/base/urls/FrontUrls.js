define([],function(){
    var FrontUrls = {

        auth:{
            'sign-in/email':'singInByEmail',
            'sign-in/trello':'singInByTrello',
            'sign-up/email':'singUpByEmail',
        },

        dashboard:{
            'dashboard':'dashboard'
        },

        report:{
            'report/:reportId':'report'
        },

        create:{
            'report-create/step1':'step1',
            'report-create/step2':'step2',
        },

    };

    return FrontUrls;
})
