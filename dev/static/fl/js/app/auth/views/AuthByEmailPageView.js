define([
    'jquery',
    'underscore',
    'backbone',

    'AuthByEmailModel',
    'frontUrls',

    'text',
    'text!/static/fl/js/app/auth/templates/authByEmailPage.html'
], function ($, _, Backbone, AuthByEmailModel, frontUrls, text, authByEmailPage) {
    var AuthByEmailPageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(authByEmailPage),
        model: new AuthByEmailModel(),

        events: {
            "click .js-auth-by-email-btn": "authByEmail"
        },

        initialize: function () {
            this.model.on('change', this.checkModel, this);
        },

        render: function () {
            this.$el.html(this.template({}));
            $('.js-auth-error').hide();
            return this.$el;
        },

        checkModel: function () {
            console.log('CheckModel:');
            if (this.model.get('code').code == 0) {
                Backbone.history.navigate('/sign-in/trello', true);
            } else {
                $('.js-auth-error').show();
            }
        },

        authByEmail: function () {
            console.log('authByEmail:');
            var email = $('#js-email').val();
            var password = $('#js-password').val();
            this.model.login(email, password);
        }
    });

    return AuthByEmailPageView;
});