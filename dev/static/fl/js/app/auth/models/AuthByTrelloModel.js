define([
    'jquery',
    'underscore',
    'backbone',

    'apiUrls',

    'MeModel'
], function ($, _, Backbone, apiUrls, meModel) {
    var AuthByTrelloModel = Backbone.Model.extend({
        defaults: {
            status: 0
        },


        authenticationSuccess: function () {
            console.log("Trello Successful authentication");
            console.log(Trello.token());
            this.set({'status': 0});
            this.set({'token': Trello.token()});

            var self = this;

            $.post(
                apiUrls.authByTrello,
                {
                    token: Trello.token()
                },
                function(data){
                    if(data.code.code == 0){
                        self.set({'status': 1});
                    }else{
                        self.set({'status': 2});
                    }
                    meModel.fetch();
                }
            );
        },

        authenticationFailure: function () {
            console.log("Trello Failed authentication");
            this.set({'status': 2});
            meModel.fetch();
        },

        startAuth: function () {
            var self = this;
            Trello.authorize({
                type: "popup",
                name: "Focus List for Trello",
                scope: {
                    read: true,
                    write: true
                },
                expiration: "never",
                success: function () {
                    self.authenticationSuccess();
                },
                error: function () {
                    self.authenticationFailure();
                }
            });
        }
    });


    return AuthByTrelloModel;
});
