define([
    'jquery',
    'underscore',
    'backbone',
    'apiUrls'
], function ($, _, Backbone, apiUrls) {
    var AuthByEmailModel = Backbone.Model.extend({
        login: function (email, password) {
            var self = this;
            this.set({
                code:{
                    code: -1
                }
            });
            $.post(
                apiUrls.authByEmail,
                {
                    'email': email,
                    'password': password
                },
                function (data) {
                    console.log("Login:")
                    console.log(data);
                    self.set(data);
                }
            );

        }
    });

    return AuthByEmailModel;
});