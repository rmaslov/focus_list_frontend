define([
    "jquery",
    "underscore",
    "backbone",
    'apiUrls'
], function ($, _, Backbone, apiUrls) {
    var MeModel = Backbone.Model.extend({
        defaults: {},

        fetch: function () {
            var self = this;
            $.get(
                apiUrls.me,
                {},
                function (data) {
                    self.set(data);
                }
            );
        },

        logout: function(){
            var self = this;
            $.get(
                apiUrls.logout,
                {},
                function (data) {
                    self.fetch();
                }
            );
        }
    });

    var meModel = new MeModel();
    meModel.fetch();

    return meModel;
});