define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'AuthByEmailPageView',
    'AuthByTrelloPageView'
], function ($, _, Backbone, frontUrls, AuthByEmailPageView, AuthByTrelloPageView) {
    var AuthRoutes = Backbone.Router.extend({
        routes: frontUrls.auth,


        singInByEmail: function () {
            var view = new AuthByEmailPageView();
            view.render();
            console.log('singInByEmail did load');
        },
        singInByTrello: function () {
            var view = new AuthByTrelloPageView();
            view.render();
            console.log('singInByTrello did load');
        },
        singUpByEmail: function () {
            console.log('singUpByEmail did load');
        },

    });

    return new AuthRoutes();
});