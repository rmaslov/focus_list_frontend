define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'ReportCreateModel',

    'text',
    'text!/static/fl/js/app/reportCreate/templates/step2.html'
], function ($, _, Backbone, frontUrls, reportCreateModel, text, stepTemplate) {
    var ReportCreateStep2PageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(stepTemplate),
        model: reportCreateModel,

        events: {
            'change .js-rc-list-type-checkbox': 'checkBoxChange',
            'click .js-report-create-finish': 'finishButtonDidClick'
        },

        checkBoxChange: function (event) {
            if ($(event.currentTarget).prop('checked') == true) {
                this.model.setListType($(event.currentTarget).attr('data-id'), $(event.currentTarget).attr('data-value'));
            }else{
                this.model.removeListType($(event.currentTarget).attr('data-id'), $(event.currentTarget).attr('data-value'));
            }
        },

        finishButtonDidClick: function(event){
            this.model.createReport();
        },

        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.loadTrelloListForBoards();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this.$el;
        },


    });

    return ReportCreateStep2PageView;
});