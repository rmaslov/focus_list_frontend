define([
    'jquery',
    'underscore',
    'backbone',
    'apiUrls',
], function ($, _, Backbone, apiUrls) {

    var DashboardModel = Backbone.Model.extend({
        defaults: {
            reports: [],
        },

        updateModel: function () {
            var self = this;

            $.ajax({
                url: apiUrls.reportAll,
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    self.set('reports', data.results);
                },
            });

        }


    });

    return DashboardModel;

});