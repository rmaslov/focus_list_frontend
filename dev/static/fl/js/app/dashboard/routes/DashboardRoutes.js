define([
    'jquery',
    'underscore',
    'backbone',

    'frontUrls',

    'DashboardPageView',
], function ($, _, Backbone, frontUrls, DashboardPageView) {
    var DashboardRoutes = Backbone.Router.extend({
        routes: frontUrls.dashboard,


        dashboard: function () {
            var view = new DashboardPageView();
            view.render();
            console.log('dashboard did load');
        },

    });

    return new DashboardRoutes();
});