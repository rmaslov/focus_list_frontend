define([
    'jquery',
    'underscore',
    'backbone',
    'c3',

    'frontUrls',

    'text',
    'text!/static/fl/js/app/report/templates/report.html',

    'ReportModel'

], function ($, _, Backbone, c3, frontUrls, text, reportTemplate, ReportModel) {
    var ReportPageView = Backbone.View.extend({
        el: $('.js-main-body'),
        template: _.template(reportTemplate),
        model: new ReportModel(),
        reportId: null,

        events: {},

        initialize: function () {
            this.model = null;
            this.model = new ReportModel();
            this.model.on('change', this.render, this);
        },

        setReportId: function (reportId) {
            this.reportId = reportId;
            this.model.updateModel(reportId);
        },

        render: function () {
            $('.js-header-loader').addClass('hide');
            this.$el.html(this.template(this.model.toJSON()));
            this.generateChart();
            this.generateChart1();
            return this.$el;
        },

        generateChart: function () {
            var chart = c3.generate({
                bindto: '.js-report-chart',
                data: {
                    x: 'x',
                    columns: [
                        ['x', 'Sapforfreelance', 'Homeadvizor.IOS', 'Bluret', 'Focus'],
                        ['data2', 130, 10, 4, 4],
                        ['data3', 30, 0, 0, 1],
                        ['data4', 430, 30, 140, 20],

                    ],

                    names: {
                        //data1: 'Data Name 1',
                        data2: 'ToDo',
                        data3: 'Doing',
                        data4: 'Done',
                    },
                    type: 'bar'
                },
                axis: {
                    x: {
                        type: 'category' // this needed to load string x value
                    }
                },
                bar: {
                    width: {
                        ratio: 0.5 // this makes bar width 50% of length between ticks
                    }
                    // or
                    //width: 100 // this makes bar width 100px
                },
                zoom: {
                    enabled: true
                }
            });
        },
        generateChart1: function () {
            var chart = c3.generate({
                bindto: '.js-report-chart1',
                data: {
                    x: 'x',
                    columns: [
                        ['x', 'ToDo', 'Doing', 'Done'],
                        ['data2', 130, 10, 304],
                        ['data3', 30, 0, 30],
                        ['data4', 30, 3, 140],

                    ],

                    names: {
                        //data1: 'Data Name 1',
                        data2: 'Sap',
                        data3: 'HA',
                        data4: 'Bluret',
                    },
                    type: 'bar'
                },
                axis: {
                    x: {
                        type: 'category' // this needed to load string x value
                    }
                },
                bar: {
                    width: {
                        ratio: 0.5 // this makes bar width 50% of length between ticks
                    }
                    // or
                    //width: 100 // this makes bar width 100px
                },
                zoom: {
                    enabled: true
                }
            });
        },


    });

    return ReportPageView;
});